package com.e.taller4;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity implements MediaController.MediaPlayerControl {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final String PRINCIPAL_CHANNEL_ID = "Main";
    private static final int NOTIFY_ID = 100;
    private static final int REQUEST_CODE = 30;

    ArrayList<Song> songList;
    RecyclerView rvsong_list;
    SongAdapter songAdapt;
    LinearLayoutManager linearLayoutManager;

    private MusicService musicServ;
    private Intent playIntent;
    private boolean musicBound = false;
    MusicController controller;

    private boolean paused = false, playbackPaused = false;

    static final String PLAY_PAUSE_ACTION = "action.PLAYPAUSE";
    static final String NEXT_ACTION = "action.NEXT";
    static final String PREV_ACTION = "action.PREV";


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createNotificationChannel(); // NECESARIO PARA LA VERSION 8.0 o mayors
        initUI();
        getSongList();
        setController();
    }

    public void createNotificationChannel() { // Para identificar el canal al que se le va a enviar la notificacion, solo muestra 1 linea
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel =
                    new NotificationChannel(PRINCIPAL_CHANNEL_ID, "Canal principal", importance);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }

    }

    private void initUI() {

        rvsong_list = findViewById(R.id.rvsong_list);

        songList = new ArrayList<Song>();

        songAdapt = new SongAdapter(songList, this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rvsong_list.setAdapter(songAdapt);
        rvsong_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        songAdapt.setOnItemClickListener(new SongAdapter.OnItemClickListerner() {// para cada elemento en la lista de canciones
            //Se establece la posición de la canción para cada elemento en la vista de lista cuando se definine
            // la clase Adapter Se recupera aquí y se pasa a la instancia Service antes de llamar al método para iniciar la reproducción

            @Override
            public void onItemClick(int position) {
                musicServ.setSong(position);
                musicServ.playSong();
                controller.show();
                not(position);
            }
        });

    }

    private PendingIntent playerAction(String action) {
        final Intent pauseIntent = new Intent();
        pauseIntent.setAction(action);
        return PendingIntent.getBroadcast(musicServ, REQUEST_CODE, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @NonNull
    private NotificationCompat.Action notificationAction(final String action) {
        int icon;
        switch (action) {
            default:
            case PREV_ACTION:
                icon = android.R.drawable.ic_media_previous;
                break;
            case PLAY_PAUSE_ACTION:
                icon = musicServ.isPng() == false ? android.R.drawable.ic_media_pause : android.R.drawable.ic_media_play;
                break;
            case NEXT_ACTION:
                icon = android.R.drawable.ic_media_next;
                break;
        }
        return new NotificationCompat.Action.Builder(icon, action, playerAction(action)).build();
    }

    private void not(int position) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //llevará al usuario a la clase principal Activity cuando seleccione la notificación
        PendingIntent pendInt = PendingIntent.getActivity(this, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, PRINCIPAL_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(songList.get(position).getSongTitle())
                .setContentText(songList.get(position).getSongTitle())
                .setContentIntent(pendInt)
                .setAutoCancel(false)
                .addAction(notificationAction(PREV_ACTION))
                .addAction(notificationAction(PLAY_PAUSE_ACTION))
                .addAction(notificationAction(NEXT_ACTION))
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle())
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(NOTIFY_ID, notification);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getSongList() { //Dentro de este método, crea una instancia de ContentResolver, se obtiene la URI para los archivos de
        // audio externos, y crea una instancia de Cursor usando la instancia de ContentResolver para buscar los archivos de música
        ContentResolver musicResolver = getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        //SOLICITAR PERMISO PARA LEER LOS ARCHIVOS DEL CEL
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "Se necesita permiso para lectura", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            return;
        }

        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

        //Validacion de datos
        if (musicCursor != null && musicCursor.moveToFirst()) {
            //obtener columnas
            int idColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
            int artistColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST);
            //agregar canciones a la lista
            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                songList.add(new Song(thisId, thisTitle, thisArtist));
            }
            while (musicCursor.moveToNext());
        }

        Collections.sort(songList, new Comparator<Song>() { //Ordenar las canciones por titulo
            public int compare(Song a, Song b) {
                return a.getSongTitle().compareTo(b.getSongTitle());
            }
        });
    }

    //conectar al servicio
    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder) service;
            //get service
            musicServ = binder.getService();
            //pass list
            musicServ.setList(songList);
            musicBound = true; // bandera booleana para realizar un seguimiento del estado del enlace
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };

    @Override
    protected void onStart() { //Cuando la actividad comienza la instancia
        super.onStart();
        if (playIntent == null) {
            playIntent = new Intent(this, MusicService.class); //creamos el objeto intent si aún no existe
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE); //lo enlazamos
            startService(playIntent); //e iniciamos
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //implementa el botón de finalización que se agregó al menú principal.
        // se agregua el método para responder a la selección de elementos del menú menu item selected
        switch (item.getItemId()) {
            case R.id.action_shuffle: //Ahora el usuario podrá usar el elemento del menú para alternar la funcionalidad de mezcla
                musicServ.setShuffle();
                break;
            case R.id.action_end:
                stopService(playIntent);
                musicServ = null;
                System.exit(0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {//Para el botón Finalizar, se detiene la instancia Service y se sale de la aplicación. Al presionar el
        // botón Atrás no saldrá de la aplicación, ya que se asume que el usuario desea que la reproducción continúe a menos que
        // seleccione el botón Finalizar. Se utiliza el mismo proceso si la aplicación se destruye, anulando el
        // onDestroy método de la actividad
        super.onDestroy();
        stopService(playIntent);
        musicServ = null;
        unbindService(musicConnection);
    }

    private BroadcastReceiver onPrepareReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent i) {
            // When music player has been prepared, show controller
            controller.show(0);
        }
    };

    private void setController() {
        controller = new MusicController(this);
        controller.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNext();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPrev();
            }
        });

        controller.setMediaPlayer(this);
        controller.setAnchorView(findViewById(R.id.rvsong_list));
        controller.setEnabled(true);
    }

    //play previous
    private void playPrev() {
        musicServ.playPrev();
        if (playbackPaused) {
            playbackPaused = false;
        }
        controller.show(0);
    }

    //play next
    private void playNext() {
        musicServ.playNext();
        if (playbackPaused) {
            playbackPaused = false;
        }
        controller.show(0);
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getCurrentPosition() {
        if (musicServ != null && musicBound && musicServ.isPng())
            return musicServ.getPosn();
        else return 0;
    }

    @Override
    public int getDuration() {
        if (musicServ != null && musicBound && musicServ.isPng())
            return musicServ.getDur();
        else return 0;
    }


    @Override
    public boolean isPlaying() {
        if (musicServ != null && musicBound)
            return musicServ.isPng();
        return false;
    }

    @Override
    public void pause() {
        playbackPaused = true;
        musicServ.pausePlayer();
    }

    @Override
    public void seekTo(int pos) {
        musicServ.seek(pos);
    }

    @Override
    public void start() {
        musicServ.go();
    }

    @Override
    protected void onPause() {
        super.onPause();
        paused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(onPrepareReceiver,
                new IntentFilter("MEDIA_PLAYER_PREPARED"));
        if (paused) {
            paused = false;
        }
    }

    @Override
    protected void onStop() {
        controller.hide();
        super.onStop();
    }

    @Override
    public int getBufferPercentage() { //Se deja por defecto
        return 0;
    }

    @Override
    public int getAudioSessionId() { //Se deja por defecto
        return 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
