package com.e.taller4;

import android.content.Context;
import android.widget.MediaController;

public class MusicController extends MediaController {
    public MusicController(Context context) {
        super(context);
    }
    public int mTimeout = 0;

    @Override
    public void show() {
        show(mTimeout);
    }

    @Override
    public void show(int timeout) {
        super.show(mTimeout);
    }
}
