package com.e.taller4;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import java.util.ArrayList;
import java.util.Random;

public class MusicService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener{

    //media player
    private MediaPlayer player;
    //song list
    private ArrayList<Song> songs;
    //current position
    private int songPos;

    private final IBinder musicBind = new MusicBinder();
    private String songTitle = "";

    private boolean shuffle=false;
    private Random rand; //instancia del generador de números aleatorios

    public MusicService() {
    }

    public void onCreate(){
        //create the service
        super.onCreate();
        //createNotificationChannel(); // NECESARIO PARA LA VERSION 8.0 o mayor
        //initialize position
        songPos=0;
        //create instance player
        player = new MediaPlayer();

        initMusicPlayer();
        rand=new Random();
    }

    public void initMusicPlayer(){
        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);//El bloqueo de activación permitirá que
        // la reproducción continúe cuando el dispositivo esté inactivo
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);//tipo de transmisión en música

        player.setOnPreparedListener(this); //cuando la MediaPlayerinstancia está preparada
        player.setOnCompletionListener(this); //cuando una canción ha completado la reproducción
        player.setOnErrorListener(this); //se produce un error
    }

    public void setList(ArrayList<Song> theSongs){ //para pasar la lista de canciones de Activity
        songs = theSongs;
    }

    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent){ //para liberar recursos cuando la instancia del Service no esté vinculada
        //Esto se ejecutará cuando el usuario salga de la aplicación, momento en el que se detendra el servicio.
        player.stop();
        player.release();
        return false;
    }

    public void playSong(){
        player.reset(); //se comienza reiniciando MediaPlayer ya que también se usará este código cuando el usuario esté reproduciendo
        // canciones posteriores

        Song playSong = songs.get(songPos); // //get song
        songTitle=playSong.getSongTitle(); // obtener el título de la canción
        long currSong = playSong.getId(); //get id
        //set uri
        Uri trackUri = ContentUris.withAppendedId( android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currSong);

        try{
            player.setDataSource(getApplicationContext(), trackUri); //intentar establecer este URI como la fuente de
            // datos para el MediaPlayer
        }
        catch(Exception e){ // Se puede producir una excepción
            Log.e("MUSIC SERVICE", "Error setting data source", e);
        }

        player.prepareAsync(); //Para preparar el MediaPlayer
    }


    @Override
    public void onPrepared(MediaPlayer mp) { // Dentro de este método, comienza la reproducción
        mp.start();
        Intent onPreparedIntent = new Intent("MEDIA_PLAYER_PREPARED");
        LocalBroadcastManager.getInstance(this).sendBroadcast(onPreparedIntent);
    }

    //Para que el usuario seleccione canciones, también se necesita un método en  el servicio
    // MusicService para configurar la canción actual
    public void setSong(int songIndex){ //Se llamará a esto cuando el usuario elija una canción de la lista
        songPos = songIndex;
    }

    //Se invocarán los métodos de la clase Activity que agregamos para implementar la interfaz MediaPlayerControl cuando el usuario
    // intente controlar la reproducción. Se necesitará la clase Service para actuar sobre este control
    public int getPosn(){
        return player.getCurrentPosition();
    }

    public int getDur(){
        return player.getDuration();
    }

    public boolean isPng(){
        return player.isPlaying();
    }

    public void pausePlayer(){
        player.pause();
    }

    public void seek(int posn){
        player.seekTo(posn);
    }

    public void go(){
        player.start();
    }

    public void playPrev(){ //Pista anterior, Disminuimos la variable del índice de la canción,
        //verificamos que no hemos salido del rango de la lista y llamamos al método playSong que agregamos
        songPos--;
        if(songPos<0) songPos=songs.size()-1;
        playSong();
    }

    //skip to next
    public void playNext(){ //activaremos y desactivaremos la configuración aleatoria. Comprobaremos este indicador cuando el usuario
        // salte a la siguiente pista o cuando una pista finalice y comience la siguiente.
            if(shuffle){ //Si la bandera de reproducción aleatoria está activada, elegimos una nueva canción de la lista al azar,
                // asegurándonos de no repetir la última canción reproducida
                int newSong = songPos;
                while(newSong==songPos){
                    newSong=rand.nextInt(songs.size());
                }
                songPos=newSong;
            }
            else{
                songPos++;
                if(songPos>=songs.size()) songPos=0;
            }
            playSong();
    }

    public void setShuffle(){ //método para establecer el indicador de reproducción aleatoria
        if(shuffle) shuffle=false;
        else shuffle=true;
    }

    @Override
    public boolean onError(MediaPlayer mp, int i, int i1) {
        mp.reset();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) { //se activará cuando finalice una pista, incluidos los casos en que el
        // usuario haya elegido una nueva pista o salteado a las pistas siguiente / anterior, así como cuando la pista llegue
        // al final de su reproducción. En este último caso, queremos continuar la reproducción tocando la siguiente pista
        if(player.getCurrentPosition()>0){
            mp.reset();
            playNext();
        }
    }
}
