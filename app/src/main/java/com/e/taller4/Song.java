package com.e.taller4;

public class Song {
    private long songID;
    private String songTitle;
    private String songArtist;

    public Song(long songID, String songTitle, String songArtist) {
        this.songID = songID;
        this.songTitle = songTitle;
        this.songArtist = songArtist;
    }

    public long getId() {
        return songID;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getSongArtist() {
        return songArtist;
    }
}
