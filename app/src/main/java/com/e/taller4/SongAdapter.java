package com.e.taller4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder>{

    ArrayList<Song> list_songs;
    Context mContext;
    private SongAdapter.OnItemClickListerner mListener;

    public SongAdapter(ArrayList<Song> list_songs, Context mContext) {
        this.list_songs = list_songs;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_canciones,parent, false);
        return new ViewHolder(mView, mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Song songTemp = list_songs.get(position);
        holder.tvSongTitle.setText(songTemp.getSongTitle());
        holder.tvSongArtist.setText(songTemp.getSongArtist());
    }

    @Override
    public int getItemCount() {
        return (list_songs!=null)?list_songs.size():0; //Retorna el tamaño de la lista si no esta vacía
    }

    public interface OnItemClickListerner {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(SongAdapter.OnItemClickListerner listener) {
        mListener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvSongTitle, tvSongArtist;

        public ViewHolder(@NonNull View itemView, final SongAdapter.OnItemClickListerner listener) {
            super(itemView);
            tvSongTitle = itemView.findViewById(R.id.tvSongTitle);
            tvSongArtist = itemView.findViewById(R.id.tvSongArtist);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

    }

}